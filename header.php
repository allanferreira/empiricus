<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<title>Empiricus</title>

	<?php wp_head(); ?>

	<link href="<?php echo bloginfo('template_url');?>/components/normalize-css/normalize.css" rel="stylesheet"/>
	<link href="<?php echo bloginfo('template_url');?>/css/main.css" rel="stylesheet"/>
</head>
<body>
	<section class="nav--bar">
		<div class="container">
			<nav class="row">
				<a href="#">
					<img class="logo" src="<?php echo bloginfo('template_url');?>/img/logo.png" alt="Empiricus"/>
				</a>
				<ul>
					<li class="hidden-sm">
						<a href="#">Cadastre-se</a>
					</li>
					<li class="hidden-sm">
						<a href="#">Ainda vale a pena comprar dólar?</a>
					</li>
					<li class="hidden-sm">
						<a href="#">Empiricus</a>
					</li>
					<li class="hidden-sm">
						<a href="#">Os analistas</a>
					</li>
					<li class="visible-sm">
						<div class="mobile-menu">
							<i class="icon--mobile-menu"></i>
						</div>
					</li>
				</ul>
			</nav>
		</div>
	</section>
	<section class="nav--mobile">
		<nav>
			<ul>
				<li></li>
				<li>
					<div class="mobile-menu">
						<i class="icon--mobile-close"></i>
					</div>
				</li>
				<li>
					<a href="#">Cadastre-se</a>
				</li>
				<li>
					<a href="#">Ainda vale a pena comprar dólar?</a>
				</li>
				<li>
					<a href="#">Empiricus</a>
				</li>
				<li>
					<a href="#">Os analistas</a>
				</li>
				<li></li>
			</ul>
		</nav>
	</section>