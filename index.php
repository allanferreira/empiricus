<?php get_header(); ?>
<section class="banner--newsletter">
		<div class="banner__text">
			<h1>
				Como ganhar dinheiro diante<br>
				das variações do Dólar?<br>
				Um guia estratégico para tempos<br>
				de incertezas.
			</h1>
			<p>
				Preencha seu email abaixo que<br>
				te enviaremos o relatório gratuito
			</p>
			<div class="newsletter">
				<form action="">
					<input type="text" placeholder="seunome@seuprovedor.com.br">
					<input type="submit" class="button" value="Receber">
				</form>
			</div>
		</div>
</section>
<section class="report content">
	<i class="icon--arrow-down"></i>
	<div class="container">
		<?php 
		query_posts( array('category_name'  => 'relatorio' ));
		while (have_posts()) : the_post();
		?>
		
		<h1><?php echo get_the_title();?></h1>
		<p><?php echo get_the_content();?></p>
		
		<?php 
		endwhile; 
		wp_reset_query();
		?>
	</div>
</section>
<section class="faq content">
	<div class="container">
		<h1 class="title--color">você saberá ainda.</h1>
		
		<?php 
		query_posts( array('category_name'  => 'faq' ));
		while (have_posts()) : the_post();
		?>
		
		<h2><?php echo get_the_title();?></h2>
		<p><?php echo get_the_content();?></p>
		
		<?php 
		endwhile; 
		wp_reset_query();
		?>
		
		<button class="button" data-scroll="0">
			<i class="icon--arrow-up"></i>
			Voltar ao topo
		</button>
	</div>
</section>
<section class="banner--base about content">
	<div class="container">
		<div class="row">
			<div class="banner__text col-7 col-sm-12">
				
				<?php 
				query_posts( array('category_name'  => 'sobre' ));
				while (have_posts()) : the_post();
				?>
				
				<h1><?php echo get_the_title();?></h1>
				<p><?php echo get_the_content();?></p>
				
				<?php 
				endwhile; 
				wp_reset_query();
				?>

				<a href="#">
					<button class="button">Saiba mais</button>
				</a>
			</div>
			<div class="flex hidden-sm">
				<img src="<?php echo bloginfo('template_url');?>/img/banner-logo.png" alt="Empiricus">
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>