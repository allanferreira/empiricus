<section class="footer">
	<div class="container">
		<div class="row extra-info">
			<div class="col-6 col-xs-12">
				<h1>Endereço</h1>
				<div class="address">
					<i class="icon--social-pin"></i>
					<address>
						Rua Joaquim Floriano, 913 - 4º andar<br>
	          			CEP: 04354-013 - Itaim Bibi. São Paulo/SP
					</address>
				</div>
			</div>
			<div class="col-3 col-xs-12">
				<h1>Rede social</h1>
				<div class="social">
					<a href="#">
						<i class="icon--social-facebook"></i>
					</a>
					<a href="#">
						<i class="icon--social-twitter"></i>
					</a>
					<a href="#">
						<i class="icon--social-in"></i>
					</a>
					<a href="#">
						<i class="icon--social-plus"></i>
					</a>
					<a href="#">
						<i class="icon--social-whatsapp"></i>
					</a>
					<a href="#">
						<i class="icon--social-mail"></i>
					</a>
				</div>
			</div>
			<div class="col-3 col-xs-12">
				<h1>Sobre nós</h1> 
				<p>Conheça mais sobre esse e<br>outros assuntos em empiricus.com.br</p>
			</div>
		</div>
		<div class="row copyright">
			<p>Ao se cadastrar você está concordando com os termos de nossa Política de Privacidade. Você pode cancelar sua inscrição a qualquer momento. Este é um serviço gratuito oferecido pela Empiricus. O cadastro em nossa newsletter diária GRÁTIS te habilita a receber este relatório. Nós NÃO compartilharemos seu endereço de e-mail com ninguém.</p>
			<p>Copyright © Empiricus Research 2016</p>
		</div>
	</div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="<?php echo bloginfo('template_url');?>/js/main.js"></script>
</body>
</html>