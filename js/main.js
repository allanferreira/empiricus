$(function(){
	//intro animate
	setTimeout(function(){
		$('.banner--newsletter .banner__text').addClass('animated--bottom animated--opacity');
	},1000)

	$('.nav--bar .mobile-menu').click(function(){
		$('.nav--mobile').addClass('visible')
	});
	$('.nav--mobile .mobile-menu').click(function(){
		$('.nav--mobile').removeClass('visible')
	});

	//data scroll control
	$('.button,i').click(function(){
		const data = $(this).data('scroll');
		
		$("html, body").stop().animate({
			scrollTop: data
		}, '500');
	});

	const init = () => {
		//window and navbar height
		const navHeight  = $('.nav--bar').innerHeight(),
			windowHeight = $(window).height()

		//fix margin body
		$('body').css({marginTop:navHeight});
		$('.banner--newsletter').height(windowHeight-navHeight)

		// set scroll
		$('.icon--arrow-down').attr('data-scroll',windowHeight-navHeight)
	}

	init()

	//fix vars in resize
	$(window).resize(function() {
		init()
	});
})